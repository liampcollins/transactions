To run application locally

1. `npm install`
2. `npm start`

To run tests

1. `npm test`

To create a production build

1. `npm run build`
