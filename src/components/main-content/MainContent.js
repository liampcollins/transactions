import React, { Component } from "react";
import Footer from "../common/footer/Footer";

class MainContent extends Component {
  render() {
    const steps = [
      {
        text: "Transaction info",
        active: true
      },
      {
        text: "Recipient info",
        active: false
      },
      {
        text: "Make payment",
        active: false
      }
    ];
    return (
      <div className="page__main-container">
        <div className="page__main">
          <div className="steps-nav">
            {steps.map((s, i) => (
              <div
                key={i}
                className={`steps-nav__step ${
                  s.active ? "steps-nav__step--active" : ""
                }`}
              >
                <div className="steps-nav__step-content">
                  <div className="steps-nav__step-title">Step {i + 1}</div>
                  <div className="steps-nav__step-subtitle">{s.text}</div>
                </div>
                <div
                  className={`steps-nav__underscore ${
                    i === 0 ? "steps-nav__underscore--first" : ""
                  } ${
                    i === steps.length - 1 ? "steps-nav__underscore--last" : ""
                  } ${s.active ? "steps-nav__underscore--active" : ""}`}
                />
              </div>
            ))}
          </div>
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}

export default MainContent;
