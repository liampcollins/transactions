import React from "react";
import PropTypes from "prop-types";
import FlagIcon from "../../common/flag-icon/FlagIcon.js";

const CurrencyTag = props => (
  <div className="currency-tag">
    <FlagIcon squared={true} code={props.country.toLowerCase()} />
    <span>{props.currency}</span>
  </div>
);

CurrencyTag.propTypes = {
  currency: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired
};
export default CurrencyTag;
