import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import CurrencyTag from "./CurrencyTag";

let props;
function mountComponent() {
  props = {
    currency: "GBP",
    country: "GB"
  };
  const wrapper = mount(<CurrencyTag {...props} />);
  return {
    props,
    wrapper
  };
}

describe("CurrencyTag", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<CurrencyTag {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render the correct currency", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find(".currency-tag").text()).toBe("GBP");
  });
});
