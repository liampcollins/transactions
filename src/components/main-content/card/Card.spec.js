import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import Card from "./Card";

let props;
function mountComponent() {
  const wrapper = mount(<Card {...props} />);
  return {
    props,
    wrapper
  };
}

describe("Card", () => {
  beforeEach(() => {
    props = {
      amount: 101.21,
      type: "test",
      order: "second",
      title: "Test Card",
      currency: "testCurr",
      country: "GB"
    };
  });

  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<Card {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe("adding classes", () => {
    it("should add the correct order class", () => {
      const { wrapper } = mountComponent();
      expect(wrapper.find(".card--second").length).toBe(1);
    });

    it("should add the type class if a type exists", () => {
      const { wrapper } = mountComponent();
      expect(wrapper.find(".card--test").length).toBe(1);
    });

    it("should add the type class if a type exists", () => {
      delete props.type;
      const { wrapper } = mountComponent();
      expect(
        wrapper
          .find(".card")
          .first()
          .props().className
      ).toBe("card card--second");
    });
  });

  describe("rendering content", () => {
    it("should render the start of the amount", () => {
      const { wrapper } = mountComponent();
      expect(
        wrapper
          .find(".card__amount")
          .children()
          .first()
          .text()
      ).toBe("101");
    });
    it("should render the decimals of the amount", () => {
      const { wrapper } = mountComponent();
      expect(
        wrapper
          .find(".card__amount")
          .children()
          .last()
          .text()
      ).toBe(".21");
    });
    it("should render the currency tag", () => {
      const { wrapper } = mountComponent();
      expect(wrapper.find("CurrencyTag").length).toBe(1);
    });
    it("should pass the correct props to the currency tag", () => {
      const { wrapper } = mountComponent();
      expect(wrapper.find("CurrencyTag").props()).toEqual({
        currency: "testCurr",
        country: "GB"
      });
    });
  });
});
