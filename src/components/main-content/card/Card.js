import React from "react";
import PropTypes from "prop-types";
import getSymbolFromCurrency from "currency-symbol-map";
import CurrencyTag from "../currency-tag/CurrencyTag";

const Card = props => {
  const typeClass = props.type ? ` card--${props.type}` : "";
  const titleTypeClass = props.type ? ` card__title--${props.type}` : "";
  return (
    <div className={`card${typeClass} card--${props.order}`}>
      <div>
        <div className={`card__title ${titleTypeClass}`}>{props.title}</div>
        <div className="card__amount">
          <span>
            <span className="card__amount-currency">
              {getSymbolFromCurrency(props.currency)}
            </span>
            {props.amount.toString().split(".")[0]}
          </span>
          <span className="card__amount-cents">
            .
            {
              props.amount
                .toFixed(2)
                .toString()
                .split(".")[1]
            }
          </span>
        </div>
      </div>
      <div>
        <CurrencyTag currency={props.currency} country={props.country} />
      </div>
    </div>
  );
};

Card.propTypes = {
  type: PropTypes.string,
  order: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  currency: PropTypes.string.isRequired
};

export default Card;
