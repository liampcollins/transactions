import React from "react";
import { calcRecipientAmt, types } from "../../../utils";
import PropTypes from "prop-types";
import Card from "../card/Card";

const TransactionSteps = props => {
  const {
    srcAmount,
    cfRate,
    cfFee,
    srcCurrency,
    srcCountry,
    destCurrency,
    destCountry
  } = props.transaction;
  const recipientGets = calcRecipientAmt(srcAmount, cfRate, cfFee);

  return (
    <div className="step">
      <div>
        <div className="step__title">Let's set up your transaction!</div>
        <div className="step__subtitle">
          Specify the amount to be sent or received
        </div>
      </div>
      <div className="step__cards-container">
        <Card
          title="You send"
          amount={srcAmount}
          currency={srcCurrency}
          country={srcCountry}
          type="highlight"
          order="first"
        />
        <Card
          title="Receiver gets"
          amount={recipientGets}
          currency={destCurrency}
          country={destCountry}
          order="last"
        />
      </div>
      <button className="btn btn--primary" onClick={props.toggleModal}>
        Next
      </button>
    </div>
  );
};

TransactionSteps.propTypes = {
  transaction: types.transaction,
  toggleModal: PropTypes.func.isRequired
};

export default TransactionSteps;
