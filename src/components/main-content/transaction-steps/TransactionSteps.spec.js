import React from "react";
import moment from "moment";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import TransactionSteps from "./TransactionSteps";
import { calcRecipientAmt } from "../../../utils";

let props;
let recipientGets;
function mountComponent() {
  props = {
    transaction: {
      srcCurrency: "EUR",
      destCurrency: "GBP",
      srcAmount: 2000.0,
      deliveryDate: moment("2018-11-25").format("Do MMMM"),
      cfRate: 0.86022,
      bankRate: 0.835875,
      cfFee: 2.5,
      bankFee: 20,
      srcCountry: "EU",
      destCountry: "GB",
      verified: false
    },
    toggleModal: jest.fn()
  };
  recipientGets = calcRecipientAmt(props.srcAmount, props.cfRate, props.cfFee);
  const wrapper = mount(<TransactionSteps {...props} />);
  return {
    props,
    wrapper
  };
}

describe("TransactionSteps", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<TransactionSteps {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render the cards", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find("Card").length).toBe(2);
  });

  it("should pass the corect props to the 'you send' card", () => {
    const { wrapper } = mountComponent();
    const cardProps = wrapper
      .find("Card")
      .first()
      .props();
    expect(cardProps).toEqual({
      title: "You send",
      amount: 2000.0,
      country: "EU",
      currency: "EUR",
      type: "highlight",
      order: "first"
    });

    it("should pass the corect props to the 'receiver gets' card", () => {
      const { wrapper } = mountComponent();
      const cardProps = wrapper
        .find("Card")
        .last()
        .props();
      expect(cardProps).toEqual({
        title: "Receiver gets",
        amount: recipientGets,
        currency: "GBP",
        order: "last"
      });
    });

    it("should pass the correct click function to the button", () => {
      let mockToggleModal = jest.fn();
      props.toggleModal = mockToggleModal;
      const buttonProps = wrapper.find("button").props();

      const { wrapper } = mountComponent();
      expect(buttonProps).toEqual({
        onClick: mockToggleModal
      });
    });
  });
});
