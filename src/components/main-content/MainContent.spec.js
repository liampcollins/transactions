import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import MainContent from "./MainContent";
import MainContentLoading from "./main-content-loading/MainContentLoading";
let props;
function mountComponent() {
  props = {
    children: <MainContentLoading />
  };
  const wrapper = mount(<MainContent {...props} />);
  return {
    props,
    wrapper
  };
}

describe("MainContent", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<MainContent {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe("class setting", () => {
    it("should render the tabs with the correct active classes", () => {
      const { wrapper } = mountComponent();
      expect(wrapper.find(".steps-nav__step").length).toBe(3);
      expect(
        wrapper
          .find(".steps-nav__step")
          .first()
          .hasClass("steps-nav__step--active")
      ).toBe(true);
      expect(
        wrapper
          .find(".steps-nav__step")
          .at(1)
          .hasClass("steps-nav__step--active")
      ).toBe(false);
      expect(
        wrapper
          .find(".steps-nav__step")
          .at(2)
          .hasClass("steps-nav__step--active")
      ).toBe(false);
      expect(wrapper.find(".steps-nav__underscore").length).toBe(3);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .first()
          .hasClass("steps-nav__underscore--active")
      ).toBe(true);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .at(1)
          .hasClass("steps-nav__underscore--active")
      ).toBe(false);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .at(2)
          .hasClass("steps-nav__underscore--active")
      ).toBe(false);
    });

    it("should render the tabs with the correct order classes", () => {
      const { wrapper } = mountComponent();
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .first()
          .hasClass("steps-nav__underscore--first")
      ).toBe(true);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .at(1)
          .hasClass("steps-nav__underscore--first")
      ).toBe(false);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .at(2)
          .hasClass("steps-nav__underscore--first")
      ).toBe(false);

      expect(
        wrapper
          .find(".steps-nav__underscore")
          .first()
          .hasClass("steps-nav__underscore--last")
      ).toBe(false);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .at(1)
          .hasClass("steps-nav__underscore--last")
      ).toBe(false);
      expect(
        wrapper
          .find(".steps-nav__underscore")
          .at(2)
          .hasClass("steps-nav__underscore--last")
      ).toBe(true);
    });
  });

  // describe("tab content", () => {
  //   it("should the correct tab title", () => {
  //     const { wrapper } = mountComponent();

  //     expect(
  //       wrapper
  //         .find(".steps-nav__step-title")
  //         .first()
  //         .text()
  //     ).toBe("Step 1");

  //     expect(
  //       wrapper
  //         .find(".steps-nav__step-title")
  //         .at(1)
  //         .text()
  //     ).toBe("Step 2");
  //     expect(
  //       wrapper
  //         .find(".steps-nav__step-title")
  //         .at(2)
  //         .text()
  //     ).toBe("Step 3");
  //   });

  //   it("should the correct tab subtitle", () => {
  //     const { wrapper } = mountComponent();

  //     expect(
  //       wrapper
  //         .find(".steps-nav__step-subtitle")
  //         .first()
  //         .text()
  //     ).toBe("Transaction info");

  //     expect(
  //       wrapper
  //         .find(".steps-nav__step-subtitle")
  //         .at(1)
  //         .text()
  //     ).toBe("Recipient info");
  //     expect(
  //       wrapper
  //         .find(".steps-nav__step-subtitle")
  //         .at(2)
  //         .text()
  //     ).toBe("Make payment");
  //   });
  // });

  // describe("child component rendering", () => {
  //   it("should render the transaction steps when loading is false", () => {
  //     const { wrapper } = mountComponent();
  //     expect(wrapper.find("MainContentLoading").length).toBe(1);
  //   });

  //   it("should render the transaction steps when loading is false", () => {
  //     const { wrapper } = mountComponent();
  //     expect(wrapper.find("Footer").length).toBe(1);
  //   });
  // });
});
