import React from "react";
import Loader from "../../common/loader/Loader";

const MainContentLoading = () => (
  <div>
    <Loader />
  </div>
);

export default MainContentLoading;
