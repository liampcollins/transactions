import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import MainContentLoading from "./MainContentLoading";

let props;
function mountComponent() {
  props = {};
  const wrapper = mount(<MainContentLoading {...props} />);
  return {
    props,
    wrapper
  };
}

describe("MainContentLoading", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<MainContentLoading {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
