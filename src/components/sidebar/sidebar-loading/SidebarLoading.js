import React from "react";
import Loader from "../../common/loader/Loader";

const SidebarLoading = () => (
  <div>
    <Loader />
  </div>
);

export default SidebarLoading;
