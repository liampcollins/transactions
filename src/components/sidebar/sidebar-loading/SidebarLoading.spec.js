import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import SidebarLoading from "./SidebarLoading";

let props;
function mountComponent() {
  props = {};
  const wrapper = mount(<SidebarLoading {...props} />);
  return {
    props,
    wrapper
  };
}

describe("SidebarLoading", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<SidebarLoading {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
