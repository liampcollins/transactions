import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import moment from "moment";
import TransactionSummary from "./TransactionSummary";
import { calcRecipientAmt } from "../../../utils";

let props;
function mountComponent() {
  props = {
    transaction: {
      srcCurrency: "EUR",
      destCurrency: "GBP",
      srcAmount: 2000.0,
      deliveryDate: moment("2018-11-25").format("Do MMMM"),
      cfRate: 0.86022,
      bankRate: 0.835875,
      cfFee: 2.5,
      bankFee: 20,
      srcCountry: "EU",
      destCountry: "GB",
      verified: false
    }
  };
  const wrapper = mount(<TransactionSummary {...props} />);
  return {
    props,
    wrapper
  };
}

describe("TransactionSummary", () => {
  let recipientGets;
  let recipientGetsFromBank;
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<TransactionSummary {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render the correct sending amount", () => {
    const { wrapper } = mountComponent();
    const row = wrapper.find(".summary-table__row").at(1);
    expect(
      row
        .find("span")
        .at(1)
        .text()
    ).toBe("€2000.00");
  });
  it("should render the correct rate", () => {
    const { wrapper } = mountComponent();
    const row = wrapper.find(".summary-table__row").at(3);
    expect(
      row
        .find("span")
        .at(1)
        .text()
    ).toBe("0.86022");
  });
  it("should render the correct rate", () => {
    const { wrapper } = mountComponent();
    const row = wrapper.find(".summary-table__row").at(4);
    expect(
      row
        .find("span")
        .at(1)
        .text()
    ).toBe("£2.50");
  });
  it("should render the correct delivery date", () => {
    const { wrapper } = mountComponent();
    const row = wrapper.find(".summary-table__row").at(5);
    expect(
      row
        .find("span")
        .at(1)
        .text()
    ).toBe("25th November");
  });
  it("should render the correct 'recipient gets' amount", () => {
    const { wrapper } = mountComponent();
    recipientGets = calcRecipientAmt(
      props.transaction.srcAmount,
      props.transaction.cfRate,
      props.transaction.cfFee
    );
    const row = wrapper.find(".summary-table__row").at(6);
    expect(
      row
        .find("span")
        .at(1)
        .text()
    ).toBe(`£${recipientGets.toString()}`);
  });
  it("should render the correct 'recipient saves' amount", () => {
    const { wrapper } = mountComponent();
    recipientGets = calcRecipientAmt(
      props.transaction.srcAmount,
      props.transaction.cfRate,
      props.transaction.cfFee
    );
    recipientGetsFromBank = calcRecipientAmt(
      props.transaction.srcAmount,
      props.transaction.bankRate,
      props.transaction.bankFee
    );
    const diff = (recipientGets - recipientGetsFromBank).toFixed(2);
    const row = wrapper.find(".summary-table__row").at(7);
    expect(
      row
        .find("span")
        .at(1)
        .text()
    ).toEqual(`£${diff}`);
  });
});
