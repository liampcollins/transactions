import React from "react";
import getSymbolFromCurrency from "currency-symbol-map";
import { calcRecipientAmt, types } from "../../../utils";
import { MdHelp } from "react-icons/md";

const TransactionSummary = props => {
  const {
    srcAmount,
    cfRate,
    cfFee,
    deliveryDate,
    bankRate,
    bankFee,
    destCurrency,
    srcCurrency
  } = props.transaction;
  let recipientGets = calcRecipientAmt(srcAmount, cfRate, cfFee);
  const recipientGetsFromBank = calcRecipientAmt(srcAmount, bankRate, bankFee);
  const diff = (recipientGets - recipientGetsFromBank).toFixed(2).toString();
  recipientGets = recipientGets.toString();
  if (!cfFee || !cfRate) return "";
  return (
    <div className="summary-table">
      <div className="summary-table__row summary-table__row--header summary-table__row--border-bottom summary-table__row--first">
        Sending Details
      </div>
      <div className="summary-table__row">
        <span>You send</span>
        <span>
          {getSymbolFromCurrency(srcCurrency)}
          {srcAmount.toFixed(2)}
        </span>
      </div>
      <div className="summary-table__row summary-table__row--header summary-table__row--border-top summary-table__row--border-bottom">
        <span>Receiving Details</span>
        <span className="summary-table__primary-text">
          As of right now
          <MdHelp />
        </span>
      </div>
      <div className="summary-table__row summary-table__row--section summary-table__row--section-top">
        <span>Rate</span>
        <span>{cfRate}</span>
      </div>
      <div className="summary-table__row summary-table__row--section">
        <span>Fee</span>
        <span>
          {getSymbolFromCurrency(destCurrency)}
          {cfFee.toFixed(2)}
        </span>
      </div>
      <div className="summary-table__row summary-table__row--section">
        <span>Delivery date</span>
        <span>{deliveryDate}</span>
      </div>
      <div className="summary-table__row summary-table__row--section  summary-table__row--section-bottom">
        <span>Recipient gets</span>
        <span>
          {getSymbolFromCurrency(destCurrency)}
          {recipientGets}
        </span>
      </div>
      <div className="summary-table__row summary-table__row--border-top summary-table__row--last summary-table__row--highlight-secondary">
        <div>
          <span>You save&nbsp;</span>
          <span>
            {getSymbolFromCurrency(destCurrency)}
            {diff}
          </span>
          <span>&nbsp;compared to your bank!</span>
        </div>
      </div>
    </div>
  );
};

TransactionSummary.propTypes = {
  transaction: types.transaction
};

export default TransactionSummary;
