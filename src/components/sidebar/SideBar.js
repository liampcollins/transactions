import React, { Component } from "react";
import Footer from "../common/footer/Footer";
class Sidebar extends Component {
  state = {};
  render() {
    return (
      <div className="page__sidebar">
        <div>{this.props.children}</div>
        <div className="page__sidebar-footer">
          <Footer />
        </div>
      </div>
    );
  }
}

export default Sidebar;
