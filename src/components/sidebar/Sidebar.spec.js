import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import Sidebar from "./Sidebar";
import SidebarLoading from "./sidebar-loading/SidebarLoading";
let props;
function mountComponent() {
  props = {
    children: <SidebarLoading />
  };
  const wrapper = mount(<Sidebar {...props} />);
  return {
    props,
    wrapper
  };
}

describe("Sidebar", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<Sidebar {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render its child components", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find("SidebarLoading").length).toBe(1);
  });
});
