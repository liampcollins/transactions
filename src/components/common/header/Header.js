import React from "react";
import logo from "../../../assets/images/logo.svg";

const Header = () => (
  <div className="page__header header">
    <img className="header__logo" src={logo} alt="Logo" />
  </div>
);

export default Header;
