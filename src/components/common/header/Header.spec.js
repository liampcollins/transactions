import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import Header from "./Header";
import logo from "../../../assets/images/logo.svg";

function mountComponent() {
  return mount(<Header />);
}

describe("Header", () => {
  it("should render", () => {
    const wrapper = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<Header />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should add the correcet image source to the image", () => {
    const wrapper = mountComponent();
    expect(wrapper.find("img").prop("src")).toEqual(logo);
  });
});
