import React from "react";

const Footer = () => (
  <div className="footer">
    <span>
      <span>&copy;</span>
      2016 Currency fair
    </span>
    <span>
      <a
        className="footer__link"
        href="https://www.currencyfair.com/support/en"
      >
        Help & Support
      </a>
      <a
        className="footer__link"
        href="https://www.currencyfair.com/trust/trust-legal-stuff/"
      >
        Legal Stuff
      </a>
    </span>
  </div>
);

export default Footer;
