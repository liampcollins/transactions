import React from "react";
import { mount } from "enzyme";
import renderer from "react-test-renderer";
import Footer from "./Footer";

let props;
function mountComponent() {
  props = {};
  const wrapper = mount(<Footer {...props} />);
  return {
    props,
    wrapper
  };
}

describe("Footer", () => {
  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should match snapshot", () => {
    const tree = renderer.create(<Footer {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
