import React, { Component } from "react";
import { connect } from "react-redux";
import { MdLock, MdRefresh, MdPhone } from "react-icons/md";
import PropTypes from "prop-types";
import { verifyTransaction } from "../../../actions";

export class IDVerificationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonDisabled: true,
      code: ["", "", "", "", "", ""],
      showError: false,
      codeSubmitted: false
    };
    this.codeLength = 6;
    this.inputRefs = [null, null, null, null, null, null];
  }
  componentDidMount() {
    if (this.inputRefs[0]) this.inputRefs[0].focus();
  }
  updateButtonDisabled = code => {
    let allInputsFilled = true;
    code.forEach((n, index) => {
      if (n === "") allInputsFilled = false;
    });
    this.setState({ buttonDisabled: !allInputsFilled });
  };
  handleChange = (event, i) => {
    const re = /[0-9a-zA-Z]+/g;
    if (!re.test(event.key)) {
      event.preventDefault();
      return;
    }

    let code = this.state.code.slice();
    code[i] = event.target.value;
    this.setState({ code });

    this.updateButtonDisabled(code);
    if (this.inputRefs[i + 1] && code[i] !== "") this.inputRefs[i + 1].focus();
  };
  handleSubmit = () => {
    this.props.verifyTransaction(this.state.code);
    this.setState({ codeSubmitted: true });
  };
  render() {
    return (
      <div>
        <div className="modal__header">
          <div className="modal__header-title">
            <MdLock />
            Identity verification required
          </div>
          <div className="modal__header-subtitle">
            For your security, we ocassionally require you to verify identity by
            entering a code sent to your mobile device.
          </div>
        </div>
        <div className="modal__content">
          <div className="code-form">
            <div className="code-form__header">
              <span>Enter the code sent via SMS to &nbsp;</span>
              <div>
                <span className="phone-number__area">
                  {this.props.number.area}
                </span>
                <span className="phone-number__line-number">
                  {this.props.number.line}
                </span>
              </div>
            </div>
            <div className="code-form__digits-container">
              {[...Array(6)].map((e, i) => (
                <input
                  maxLength="1"
                  ref={input => {
                    this.inputRefs[i] = input;
                  }}
                  key={i}
                  className="digit-input"
                  onChange={e => this.handleChange(e, i)}
                />
              ))}
            </div>
            <div className="code-form__actions">
              <a className="modal__link">
                <MdRefresh />
                Receive a new code
              </a>
              <a className="modal__link">
                <MdPhone />
                Receive code via call instead
              </a>
            </div>
          </div>
        </div>
        {this.state.codeSubmitted &&
          !this.props.transaction.verified &&
          !this.props.loading && (
            <div className="error">Incorrect Code Submitted</div>
          )}
        <div className="modal__footer">
          <span>
            <button
              disabled={this.state.buttonDisabled || this.props.loading}
              className="btn btn--primary btn--modal"
              onClick={this.handleSubmit}
            >
              Verify Identity
            </button>
            <button
              className="btn btn--modal"
              onClick={this.props.toggleModal}
              disabled={this.props.loading}
            >
              Back
            </button>
          </span>
          <span className="footer__link">
            I can't access this mobile device
          </span>
        </div>
      </div>
    );
  }
}

IDVerificationModal.propTypes = {
  number: PropTypes.shape({
    area: PropTypes.string.isRequired,
    line: PropTypes.string.isRequired
  }),
  toggleModal: PropTypes.func.isRequired
};

const mapStateToProps = ({ loading, transaction }) => ({
  loading,
  transaction
});

export default connect(
  mapStateToProps,
  { verifyTransaction }
)(IDVerificationModal);
