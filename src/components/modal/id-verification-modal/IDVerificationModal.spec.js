import React from "react";
import { shallow } from "enzyme";
import moment from "moment";
import renderer from "react-test-renderer";
import IDVerificationModal from "./IDVerificationModal";
import configureStore from "redux-mock-store";
const initialState = {};
const mockStore = configureStore();

let props;
let instance;
let store;
function mountComponent() {
  store = mockStore(initialState);

  const wrapper = shallow(
    <IDVerificationModal store={store} {...props} />
  ).dive();
  instance = wrapper.instance();
  instance.numbers = [];
  instance.inputRefs = [{ focus: jest.fn() }];
  return {
    props,
    wrapper
  };
}

describe("IDVerificationModal", () => {
  beforeEach(() => {
    props = {
      number: {
        area: "123",
        line: "4567"
      },
      loading: false,
      transaction: {
        srcCurrency: "EUR",
        destCurrency: "GBP",
        srcAmount: 2000.0,
        deliveryDate: moment("2018-11-25").format("Do MMMM"),
        cfRate: 0.86022,
        bankRate: 0.835875,
        cfFee: 2.5,
        bankFee: 20,
        srcCountry: "EU",
        destCountry: "GB",
        verified: false
      },
      toggleModal: jest.fn()
    };
  });
  it("should render", () => {
    const { wrapper } = mountComponent();
  });
  it("should match snapshot", () => {
    const tree = renderer
      .create(<IDVerificationModal store={store} {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("should render the correct area code", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find(".phone-number__area").text()).toBe("123");
  });
  it("should render the correct line number", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find(".phone-number__line-number").text()).toBe("4567");
  });
  it("should render the correct number of inputs", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find(".digit-input").length).toBe(6);
  });

  describe("handleChange", () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent()["wrapper"];
    });

    it("should not add a digit that isn't alphanumeric", () => {
      let event = {
        key: ",",
        target: {
          value: ","
        },
        preventDefault: jest.fn()
      };
      instance.handleChange(event, 0);
      expect(event.preventDefault).toHaveBeenCalled();
    });

    it("should add a digit that is alphanumeric", () => {
      let event = {
        key: "A",
        target: {
          value: "A"
        },
        preventDefault: jest.fn()
      };
      instance.handleChange(event, 0);
      event.key = "1";
      instance.handleChange(event, 0);
      expect(event.preventDefault).not.toHaveBeenCalled();
    });

    it("should update the code", () => {
      let event = {
        key: "A",
        target: {
          value: "A"
        }
      };
      wrapper.setState({
        code: ["A", "A", "A", "A", "", ""]
      });
      instance.handleChange(event, 5);
      expect(wrapper.state().code).toEqual(["A", "A", "A", "A", "", "A"]);
    });

    it("should leave the button disabled if some inputs are still empty", () => {
      let event = {
        key: "A",
        target: {
          value: "A"
        }
      };
      wrapper.setState({
        code: ["A", "A", "A", "A", "", ""]
      });
      instance.handleChange(event, 5);
      expect(wrapper.state().buttonDisabled).toBe(true);
    });

    it("should set the button enabled if all ipnuts have values", () => {
      let event = {
        key: "A",
        target: {
          value: "A"
        }
      };
      wrapper.setState({
        code: ["A", "A", "A", "A", "A", ""]
      });
      instance.handleChange(event, 5);
      expect(wrapper.state().buttonDisabled).toBe(false);
    });

    it("should add focus to the next element if it exists", () => {
      let event = {
        key: "A",
        target: {
          value: "A"
        }
      };
      instance.handleChange(event, 0);
    });
  });
});
