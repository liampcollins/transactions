import React from "react";
import { mount } from "enzyme";
import ModalContainer from "./ModalContainer";
import ReactModal from "react-modal";

let props;
function mountComponent() {
  const wrapper = mount(<ModalContainer {...props} />);
  return {
    props,
    wrapper
  };
}

describe("ModalContainer", () => {
  beforeEach(() => {
    props = {
      showModal: true
    };
  });

  it("should render", () => {
    const { wrapper } = mountComponent();
  });
  it("should render the ReactModal component", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find(ReactModal).length).toBe(1);
  });
  it("should render the ReactModal content", () => {
    props.children = "Test123";
    const { wrapper } = mountComponent();
    const portalNode = wrapper.find(ReactModal).text();
    expect(portalNode).toEqual("Test123");
  });
});
