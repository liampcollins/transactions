import React from "react";
import ReactModal from "react-modal";
import PropTypes from "prop-types";

const ModalContainer = props => {
  ReactModal.setAppElement("body");
  const modalStyles = {
    content: {
      width: "600px",
      height: "484px",
      background: "#FFFFFF",
      left: "50%",
      top: "50%",
      padding: "0",
      transform: "translate(-50%, -50%)"
    },
    overlay: {
      background: "rgba(0,0,0,0.8)"
    }
  };
  return (
    <div>
      <ReactModal isOpen={props.showModal} style={modalStyles}>
        {props.children}
      </ReactModal>
    </div>
  );
};

ModalContainer.propTypes = {
  showModal: PropTypes.bool.isRequired
};

export default ModalContainer;
