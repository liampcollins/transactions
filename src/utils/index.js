import PropTypes from "prop-types";

export const calcRecipientAmt = (srcAmount, rate, fee) => {
  return srcAmount * rate - fee;
};

export const types = {
  transaction: PropTypes.shape({
    srcCurrency: PropTypes.string.isRequired,
    srcCountry: PropTypes.string.isRequired,
    destCurrency: PropTypes.string.isRequired,
    destCountry: PropTypes.string.isRequired,
    srcAmount: PropTypes.number.isRequired,
    deliveryDate: PropTypes.string.isRequired,
    cfRate: PropTypes.number,
    bankRate: PropTypes.number,
    cfFee: PropTypes.number,
    bankFee: PropTypes.number,
    verified: PropTypes.bool.isRequired
  }),
  user: PropTypes.shape({
    phoneNumber: PropTypes.shape({
      area: PropTypes.string.isRequired,
      line: PropTypes.string.isRequired
    })
  })
};
