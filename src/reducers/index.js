import { combineReducers } from "redux";
import transaction from "./transaction/transaction_reducer";
import user from "./user/user_reducer";
import loading from "./loading/loading_reducer";

export default combineReducers({
  transaction,
  user,
  loading
});
