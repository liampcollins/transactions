export default (state = false, action) => {
  return action.type.indexOf("request") >= 0;
};
