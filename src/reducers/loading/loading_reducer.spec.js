import reducer from "./loading_reducer";
import * as types from "../../actions/types";
describe("loading reducer", () => {
  it("should return true if the action is a request action", () => {
    expect(reducer([], { type: types.GET_RATES_AND_FEES_REQUEST })).toEqual(
      true
    );
  });
  it("should return false if the action is a success action", () => {
    expect(reducer([], { type: types.GET_RATES_AND_FEES_SUCCESS })).toEqual(
      false
    );
  });
  it("should return false if the action is a failure action", () => {
    expect(reducer([], { type: types.GET_RATES_AND_FEES_FAILURE })).toEqual(
      false
    );
  });
});
