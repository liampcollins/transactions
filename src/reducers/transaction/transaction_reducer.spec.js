import reducer from "./transaction_reducer";
import * as types from "../../actions/types";
describe("loading reducer", () => {
  let initialState;
  beforeEach(() => {
    initialState = {
      srcCurrency: "EUR",
      destCurrency: "GBP",
      srcAmount: 2000.0,
      deliveryDate: "2018-11-25"
    };
  });
  it("should return the existing state if the action is a request action", () => {
    expect(
      reducer(initialState, { type: types.GET_RATES_AND_FEES_REQUEST })
    ).toEqual(initialState);
  });
  it("should return update the state with the new payload if the action is a success action", () => {
    expect(
      reducer(initialState, {
        type: types.GET_RATES_AND_FEES_SUCCESS,
        payload: { test: 123 }
      })
    ).toEqual({
      srcCurrency: "EUR",
      destCurrency: "GBP",
      srcAmount: 2000.0,
      deliveryDate: "2018-11-25",
      test: 123
    });
  });
  it("should return the existing state if the action is a failure action", () => {
    expect(
      reducer(initialState, { type: types.GET_RATES_AND_FEES_FAILURE })
    ).toEqual(initialState);
  });
});
