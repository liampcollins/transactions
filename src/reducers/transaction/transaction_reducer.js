import {
  GET_RATES_AND_FEES_REQUEST,
  GET_RATES_AND_FEES_SUCCESS,
  GET_RATES_AND_FEES_FAILURE,
  VERIFY_TRANSACTION_REQUEST,
  VERIFY_TRANSACTION_SUCCESS,
  VERIFY_TRANSACTION_FAILURE
} from "../../actions/types";
import moment from "moment";

const INITIAL_STATE = {
  srcCurrency: "EUR",
  srcCountry: "EU",
  destCurrency: "GBP",
  destCountry: "GB",
  srcAmount: 2000.0,
  deliveryDate: moment("2018-11-25").format("Do MMMM")
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_RATES_AND_FEES_REQUEST:
    case VERIFY_TRANSACTION_REQUEST:
      return state;
    case GET_RATES_AND_FEES_SUCCESS:
    case VERIFY_TRANSACTION_SUCCESS:
      return { ...state, ...action.payload };
    case GET_RATES_AND_FEES_FAILURE:
    case VERIFY_TRANSACTION_FAILURE:
      return state;
    default:
      return state;
  }
};
