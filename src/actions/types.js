export const GET_RATES_AND_FEES_REQUEST = "get_rates_and_fees_request";
export const GET_RATES_AND_FEES_SUCCESS = "get_rates_and_fees_success";
export const GET_RATES_AND_FEES_FAILURE = "get_rates_and_fees_failure";
export const VERIFY_TRANSACTION_REQUEST = "verify_transaction_request";
export const VERIFY_TRANSACTION_SUCCESS = "verify_transaction_success";
export const VERIFY_TRANSACTION_FAILURE = "verify_transaction_failure";
