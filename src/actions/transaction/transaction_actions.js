import {
  GET_RATES_AND_FEES_REQUEST,
  GET_RATES_AND_FEES_SUCCESS,
  GET_RATES_AND_FEES_FAILURE,
  VERIFY_TRANSACTION_REQUEST,
  VERIFY_TRANSACTION_SUCCESS,
  VERIFY_TRANSACTION_FAILURE
} from "../types";
import {
  fetchRatesAndFees,
  verifyTransactionCode
} from "../../api/transaction";

export function getRatesAndFees() {
  return dispatch => {
    dispatch({ type: GET_RATES_AND_FEES_REQUEST });
    fetchRatesAndFees()
      .then(rates => {
        dispatch({ type: GET_RATES_AND_FEES_SUCCESS, payload: rates });
      })
      .catch(err => {
        console.log("API Error - ", err);
        dispatch({ type: GET_RATES_AND_FEES_FAILURE });
      });
  };
}

export function verifyTransaction(code) {
  return dispatch => {
    dispatch({ type: VERIFY_TRANSACTION_REQUEST });
    verifyTransactionCode(code)
      .then(resp => {
        dispatch({ type: VERIFY_TRANSACTION_SUCCESS, payload: resp });
      })
      .catch(err => {
        console.log("API Error - ", err);
        dispatch({ type: VERIFY_TRANSACTION_FAILURE });
      });
  };
}
