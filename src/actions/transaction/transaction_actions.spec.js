import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as actions from "./transaction_actions";
import * as types from "../types";
import * as api from "../../api/transaction";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore();

describe("transaction action", () => {
  beforeEach(() => {
    store.clearActions();
  });

  it("should create an action to get rates and fees", async () => {
    const payload = { mypayload: 123 };
    api.fetchRatesAndFees = jest.fn(() => {
      return Promise.resolve(payload);
    });

    const expectedSuccessActions = [
      {
        type: types.GET_RATES_AND_FEES_REQUEST
      },
      {
        type: types.GET_RATES_AND_FEES_SUCCESS,
        payload
      }
    ];
    const expectedFailureActions = [
      {
        type: types.GET_RATES_AND_FEES_REQUEST
      },
      {
        type: types.GET_RATES_AND_FEES_FAILURE,
        payload
      }
    ];

    return Promise.resolve(store.dispatch(actions.getRatesAndFees()))
      .then(() => {
        expect(store.getActions()).toEqual(expectedSuccessActions);
      })
      .catch(err => {
        expect(store.getActions()).toEqual(expectedFailureActions);
      });
  });

  it("should create an action to update the transaction verification", async () => {
    const payload = { mypayload: 123 };
    api.verifyTransactionCode = jest.fn(() => {
      return Promise.resolve(payload);
    });

    const expectedSuccessActions = [
      {
        type: types.VERIFY_TRANSACTION_REQUEST
      },
      {
        type: types.VERIFY_TRANSACTION_SUCCESS,
        payload
      }
    ];
    const expectedFailureActions = [
      {
        type: types.VERIFY_TRANSACTION_REQUEST
      },
      {
        type: types.VERIFY_TRANSACTION_FAILURE,
        payload
      }
    ];

    return Promise.resolve(store.dispatch(actions.verifyTransaction()))
      .then(() => {
        expect(store.getActions()).toEqual(expectedSuccessActions);
      })
      .catch(err => {
        expect(store.getActions()).toEqual(expectedFailureActions);
      });
  });
});
