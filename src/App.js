import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./App.css";
import Header from "./components/common/header/Header";
import Sidebar from "./components/sidebar/Sidebar";
import TransactionSteps from "./components/main-content/transaction-steps/TransactionSteps";
import { getRatesAndFees } from "./actions";
import MainContent from "./components/main-content/MainContent";
import MainContentLoading from "./components/main-content/main-content-loading/MainContentLoading";
import SidebarLoading from "./components/sidebar/sidebar-loading/SidebarLoading";
import TransactionSummary from "./components/sidebar/transaction-summary/TransactionSummary";
import ModalContainer from "./components/modal/modal-container/ModalContainer";
import IDVerificationModal from "./components/modal/id-verification-modal/IDVerificationModal";
import { types } from "./utils";
export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
    this.toggleModal = this.toggleModal.bind(this);
  }
  componentDidMount() {
    this.props.getRatesAndFees();
  }
  toggleModal() {
    this.setState({
      showModal: !this.state.showModal
    });
  }
  render() {
    return (
      <div className="page">
        <Header />
        <div className="page__content">
          <MainContent>
            {this.props.loading && <MainContentLoading />}
            {!this.props.loading && (
              <TransactionSteps
                transaction={this.props.transaction}
                toggleModal={this.toggleModal}
              />
            )}
          </MainContent>
          <Sidebar>
            {this.props.loading && <SidebarLoading />}
            {!this.props.loading && (
              <TransactionSummary transaction={this.props.transaction} />
            )}
          </Sidebar>
        </div>
        <ModalContainer showModal={this.state.showModal}>
          <IDVerificationModal
            toggleModal={this.toggleModal}
            number={this.props.user.phoneNumber}
          />
        </ModalContainer>
      </div>
    );
  }
}

App.propTypes = {
  loading: PropTypes.bool,
  transaction: types.transaction,
  user: types.user
};

const mapStateToProps = ({ loading, transaction, user }) => ({
  loading,
  transaction,
  user
});

export default connect(
  mapStateToProps,
  { getRatesAndFees }
)(App);
