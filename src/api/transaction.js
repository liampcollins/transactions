// MOCK API REQUEST FOR RATES AND FEES
export function fetchRatesAndFees() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        cfRate: 0.86022,
        bankRate: 0.835875,
        cfFee: 2.5,
        bankFee: 20
      });
    }, 1000);
  });
}

export function verifyTransactionCode(code) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        verified: true
      });
    }, 1000);
  });
}
