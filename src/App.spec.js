import React from "react";
import { shallow } from "enzyme";
import { App } from "./App";
import moment from "moment";

let props;
function mountComponent() {
  const wrapper = shallow(<App {...props} />);
  return {
    props,
    wrapper
  };
}

describe("App", () => {
  beforeEach(() => {
    props = {
      getRatesAndFees: jest.fn(),
      loading: false,
      transaction: {
        srcCurrency: "EUR",
        destCurrency: "GBP",
        srcAmount: 2000.0,
        deliveryDate: moment("2018-11-25").format("Do MMMM"),
        cfRate: 0.86022,
        bankRate: 0.835875,
        cfFee: 2.5,
        bankFee: 20,
        srcCountry: "EU",
        destCountry: "GB",
        verified: false
      },
      user: {
        phoneNumber: {
          area: "+353",
          line: "872251177"
        }
      }
    };
  });

  it("should render", () => {
    const { wrapper } = mountComponent();
  });

  it("should render the main section loader when loading is true", () => {
    props.loading = true;
    const { wrapper } = mountComponent();
    expect(wrapper.find("MainContentLoading").length).toBe(1);
  });

  it("should render the transaction steps when loading is false", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find("TransactionSteps").length).toBe(1);
  });

  it("should pass the corect props to the transaction steps component", () => {
    const { wrapper } = mountComponent();
    const tsProps = wrapper.find("TransactionSteps").props();
    expect(tsProps.transaction).toEqual(props.transaction);
    expect(tsProps.toggleModal).toEqual(wrapper.instance().toggleModal);
  });

  it("should render the sidebar loader when loading is true", () => {
    props.loading = true;
    const { wrapper } = mountComponent();
    expect(wrapper.find("SidebarLoading").length).toBe(1);
  });

  it("should render the summary section when loading is false", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find("TransactionSummary").length).toBe(1);
  });

  it("should pass the corect props to the transaction summary component", () => {
    const { wrapper } = mountComponent();
    const tsProps = wrapper.find("TransactionSummary").props();
    expect(tsProps.transaction).toEqual(props.transaction);
  });

  it("should render the modal container", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.find("ModalContainer").length).toBe(1);
  });

  it("should fetch the rates from the api when the component will mount", () => {
    let mockGetRatesAndFees = jest.fn();
    props.getRatesAndFees = mockGetRatesAndFees;
    const { wrapper } = mountComponent();
    expect(mockGetRatesAndFees).toHaveBeenCalled();
  });

  it("should set show modal to false initially", () => {
    const { wrapper } = mountComponent();
    expect(wrapper.state().showModal).toBe(false);
  });

  it("should set show modal to true after toggleModal is called", () => {
    const { wrapper } = mountComponent();
    wrapper.instance().toggleModal();
    expect(wrapper.state().showModal).toBe(true);
  });
});
